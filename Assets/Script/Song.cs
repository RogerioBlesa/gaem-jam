﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NoteType
{
	C      = 0,
	CSharp = 1,
	D      = 2,
	DSharp = 3,
	E      = 4,
	F      = 5,
	FSharp = 6,
	G      = 7,
	GSharp = 8,
	A      = 9,
	ASharp = 10,
	B      = 11,
	None,
}

public enum ScaleKind
{
	Major,
	Minor,
	MelodicMinor,
	HarmonicMinor,
    Chromatic
}

public static class ScaleExtensionUtils 
{
	public static List<int> ScaleKindToSemitones(this ScaleKind scaleKind, NoteType rootNote = NoteType.C)
	{
		List<int> semitones = new List<int>();

        switch (scaleKind)
        {

            case ScaleKind.Major: semitones = new List<int> { 0, 2, 4, 5, 7, 9, 11 }; break;
            case ScaleKind.Minor: semitones = new List<int> { 0, 2, 3, 5, 7, 8, 10 }; break;
            case ScaleKind.MelodicMinor: semitones = new List<int> { 0, 2, 3, 5, 7, 9, 11 }; break;
            case ScaleKind.HarmonicMinor: semitones = new List<int> { 0, 2, 3, 5, 7, 8, 11 }; break;
            case ScaleKind.Chromatic: semitones = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}; break;

		}
		
		for (int i = 0; i < semitones.Count; i++)
		{
			semitones[i] += (int) rootNote;
		}

		return semitones;
	}
	
	public static bool IsInTheScale(ScaleKind scaleKind, int semitones)
	{
		return scaleKind.ScaleKindToSemitones().Contains(semitones%12);
	}
}

public class Song
{
	// List with the note and current time in song
	public List<(NoteType, float)> songEvents;
	public ScaleKind songScale;
	public NoteType songNote;

	public Song()
	{
		// Example song in C major
		songEvents = new List<(NoteType, float)> 
		{
			(NoteType.None,0f),
			(NoteType.B,2f),
			(NoteType.D,3f),
			(NoteType.C,4f),
			// (NoteType.B,6f),
			// (NoteType.F,8f),
			// (NoteType.GSharp,10f),
			// (NoteType.A,12f),
			// (NoteType.D,15f),
			// (NoteType.F,19f),
		};

		songScale = ScaleKind.Major;
		songNote = NoteType.C;
	}
}
