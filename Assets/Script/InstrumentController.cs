﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstrumentController : MonoBehaviour
{
    float _semitoneUpFreq;// = 1.05946309436f;
    float _semitoneUpLen;// = 1f / semitoneUpFreq;

    Color[] _noteColors = new Color[12] {
        Color.yellow,                           //E
        new Color(26/255f, 131/255f, 201/255f), //F
        new Color(47/255f, 163/255f, 136/255f), //F#
        new Color(237/255f, 123/255f, 9/255f),  //G
        new Color(143/255f, 71/255f, 43/255f),  //G#
        Color.white,                            //A
        Color.gray,                             //A#
        new Color(113/255f, 4/255f, 209/255f),  //B
        Color.red,                              //C
        new Color(184/255f, 48/255f, 165/255f), //C#
        Color.green,                            //D
        new Color(135/255f, 255/255f, 43/255f), //D#

    };
    [SerializeField] GameObject player1, player2;
    float _p1=0, _p2=1;

    float threshold = 0.001f;
    float intervalThreshold = 0.007f; //0.01 is A5; 0.005 is A6 //0.004 is D7 //0.0042 is C#7 what? //0.0045 is C7 //0.0036 works for E7 //0.007 is E6, perfect

    int _turn = 0;

    List<float> _p1Frets;
    List<float> _p2Frets;

    [SerializeField] GameObject _spriteTemplate;
    List<GameObject> _fretSprites;

    //ScaleKind currentScale = ScaleKind.HarmonicMinor;
    ScaleKind currentScale = ScaleKind.Chromatic;

    public int getSemitone(float length) {
        return findClosestSemitone(getFrequency(length));
    }
    public int getCurrentSemitone() {
        return findClosestSemitone(getCurrentNoteFrequency());
    }

    public Color semitoneToColor(int semitone) {
        if (semitone < 0) return Color.black;
        return _noteColors[semitone % 12];
    }

    // Start is called before the first frame update
    void Start()
    {
        _fretSprites = new List<GameObject>();
        for (int i = 0; i < 100; i++) {
            var inst = GameObject.Instantiate(_spriteTemplate);
            inst.SetActive(false);
            _fretSprites.Add(inst);
        }

        _semitoneUpFreq = 1.05946309436f;
        _semitoneUpLen = 1f / _semitoneUpFreq;

        Debug.Log("Interval length of 5 octaves: "+Mathf.Pow(_semitoneUpLen, 12*5));

        drawFretPositions_P1();
        drawFretPositions_P2();
        updateFretSprites();
    }

    public float getLength() {
        return Mathf.Abs(_p1 - _p2);
    }
    public float getFrequency(float length) {
        return 164.81f * 1f / length;
    }
    //!FIXME 164.81 is the frequency of E3, the lowest open guitar string
    public float getCurrentNoteFrequency() {
        return getFrequency(getLength());
    }

    public float findSemitoneFrequency(int semitone)
    {
        return Mathf.Pow(_semitoneUpFreq, semitone);
    }
    public int findClosestSemitone(float freq)
    {
        float curFreq = 164.81f; //E3
        int curSemitone = 0;
        float prevDif = Mathf.Abs(curFreq - freq);
        //Debug.Log("prev " + prevDif);

        curSemitone = 52; //E3
        curFreq = findSemitoneFrequency(1) * curFreq;
        float curDif = Mathf.Abs(curFreq - freq);
        //Debug.Log("cur " + curDif);
        while (curDif < prevDif)
        {
            prevDif = curDif;

            curFreq *= findSemitoneFrequency(1);
            curDif = Mathf.Abs(curFreq - freq);

            curSemitone++;

            if (curSemitone > 100) { Debug.LogError("shit"); break; }
        }
        return curSemitone;
    }

    void updatePlayerPositions() {
        player1.transform.position = new Vector3(_p1, 0, 0);
        player2.transform.position = new Vector3(_p2, 0, 0);
    }
    void drawFretPositions_P1()
    {
        _p1Frets = new List<float>();

        int sanityCheck = 0;

        float curp1 = _p1;
        float curLen;
        curLen = _p2 - _p1;

        while (curp1 >= 0-threshold)
        {
            int fretsemitone = getSemitone(Mathf.Abs(curp1 - _p2));
            if (ScaleExtensionUtils.IsInTheScale(currentScale, fretsemitone))
            {
                _p1Frets.Insert(0, curp1);
                Debug.DrawLine(new Vector3(curp1, 0.01f, 0), new Vector3(curp1, -0.01f, 0), Color.green);
            }

            float intervalLen = Mathf.Abs(curLen - curLen / _semitoneUpLen);
            curLen = curLen / _semitoneUpLen;
            curp1 = _p2 - curLen;

            sanityCheck++;
            if (sanityCheck >= 100) { Debug.LogError("Insane"); break; }
            if (intervalLen < intervalThreshold) { Debug.Log("Breaking due to small interval."); break; }
        }
        //Debug.Log("Curp1 on leaving while: "+curp1);

        sanityCheck = 0;
        curLen = (_p2 - _p1)*_semitoneUpLen;
        curp1 = _p2 - curLen;
        
        while (curp1 <= (_p1 + (_p2 - _p1) / 2f) + threshold) //stops at octave
        {
            int fretsemitone = getSemitone(Mathf.Abs(curp1 - _p2));
            if (ScaleExtensionUtils.IsInTheScale(currentScale, fretsemitone))
            {
                _p1Frets.Add(curp1);
                //Debug.Log("Drawing at curp1: " + curp1);
                Debug.DrawLine(new Vector3(curp1, 0.01f, 0), new Vector3(curp1, -0.01f, 0), Color.yellow);
            }
            float intervalLen = Mathf.Abs(curLen - curLen * _semitoneUpLen);
            curLen = curLen * _semitoneUpLen;
            curp1 = _p2 - curLen;

            sanityCheck++;
            if (sanityCheck >= 100) { Debug.LogError("Insane"); break; }
            if (intervalLen < intervalThreshold) { Debug.Log("Breaking due to small interval."); break; }
        }

    }
    void drawFretPositions_P2()
    {
        _p2Frets = new List<float>();
        int sanityCheck = 0;

        float curp2 = _p2;
        float curLen;
        curLen = Mathf.Abs(_p2 - _p1);

        while (curp2 <= 1 + threshold)
        {
            int fretsemitone = getSemitone(Mathf.Abs(curp2 - _p1));
            if (ScaleExtensionUtils.IsInTheScale(currentScale, fretsemitone))
            {
                _p2Frets.Add(curp2);
                Debug.DrawLine(new Vector3(curp2, 0.01f, 0), new Vector3(curp2, -0.01f, 0), Color.cyan);
            }
            float intervalLen = Mathf.Abs(curLen - curLen / _semitoneUpLen);
            curLen = curLen / _semitoneUpLen;
            curp2 = _p1 + curLen;

            sanityCheck++;
            if (sanityCheck >= 100) { Debug.LogError("Insane"); break; }
            if (intervalLen < intervalThreshold) { Debug.Log("Breaking due to small interval."); break; }
        }

        sanityCheck = 0;
        
        curLen = Mathf.Abs(_p2 - _p1) * _semitoneUpLen;
        curp2 = _p1 + curLen;

        while (curp2 >= (_p2 - Mathf.Abs(_p2 - _p1) / 2f) - threshold) //stops at octave
        {
            int fretsemitone = getSemitone(Mathf.Abs(curp2 - _p1));
            if (ScaleExtensionUtils.IsInTheScale(currentScale, fretsemitone))
            {
                _p2Frets.Insert(0, curp2);
                //Debug.Log("Drawing at curp1: " + curp2);
                Debug.DrawLine(new Vector3(curp2, 0.01f, 0), new Vector3(curp2, -0.01f, 0), Color.magenta);
            }
            float intervalLen = Mathf.Abs(curLen - curLen*_semitoneUpLen);
            curLen = curLen * _semitoneUpLen;
            curp2 = _p1 + curLen;

            sanityCheck++;
            if (sanityCheck >= 100) { Debug.LogError("Insane"); break; }
            if (intervalLen < intervalThreshold) { Debug.Log("Breaking due to small interval."); break; }
        }

    }

    void printSemitone(float length) {
        float freq = 164.81f * 1 / length;
    }

    void setSemitoneUp_P1()
    {
        if (_p1Frets != null)
        {
            float prevp1 = _p1;
            int index = getClosestFretIndex(_p1, _p1Frets);
            index = Mathf.Min(index + 1, _p1Frets.Count - 1);
            _p1 = _p1Frets[index];
            if (getCurrentSemitone() > 88) _p1 = prevp1; 
            return;
        }

        float curLen = Mathf.Abs(_p2 - _p1);
        curLen *= _semitoneUpLen;

        if (_p2 - curLen <= _p2) _p1 = _p2 - curLen; //always true lol maybe big mistake but maybe not?

    }

    void setSemitoneDown_P1()
    {
        if (_p1Frets != null)
        {
            float prevp1 = _p1;
            int index = getClosestFretIndex(_p1, _p1Frets);
            index = Mathf.Max(index - 1, 0);
            _p1 = _p1Frets[index];
            if (getCurrentSemitone() > 88) _p1 = prevp1;
            return;
        }
        float curLen = Mathf.Abs(_p2 - _p1);
        curLen /= _semitoneUpLen;

        if (_p2 - curLen >= 0 - threshold) _p1 = _p2 - curLen;
    }

    void setSemitoneUp_P2()
    {
        if (_p2Frets != null)
        {
            float prevp2 = _p2;
            int index = getClosestFretIndex(_p2, _p2Frets);
            index = Mathf.Max(index - 1, 0);
            _p2 = _p2Frets[index];
            if (getCurrentSemitone() > 88) _p2 = prevp2;
            return;
        }

        float curLen = Mathf.Abs(_p2 - _p1);
        curLen *= _semitoneUpLen;

        if (_p1 + curLen >= _p1) _p2 = _p1 + curLen; //always true lol maybe big mistake but maybe not?
    }

    void setSemitoneDown_P2()
    {
        if (_p2Frets != null)
        {
            float prevp2 = _p2;
            int index = getClosestFretIndex(_p2, _p2Frets);
            index = Mathf.Min(index + 1, _p2Frets.Count - 1);
            _p2 = _p2Frets[index];
            if (getCurrentSemitone() > 88) _p2 = prevp2;
            return;
        }
        
        float curLen = Mathf.Abs(_p2 - _p1);
        curLen /= _semitoneUpLen;

        if (_p1 + curLen <= 1 + threshold) _p2 = _p1 + curLen;


    }

    public int getTurn() {
        return _turn;
    }
    public void changeTurn() {
        _turn ^= 1;
        updateFretSprites();
    }

    int getClosestFretIndex(float mouseX, List<float> frets) {
        float dist, point;
        point = frets[0];
        dist = Mathf.Abs(mouseX - frets[0]);
        int index = 0;

        for (int i = 0; i < frets.Count; i++)
        {
            if (Mathf.Abs(mouseX - frets[i]) < dist)
            {
                dist = Mathf.Abs(mouseX - frets[i]);
                point = frets[i];
                index = i;
            }
        }

        return index;
    }
    float snapClosestFret(float mouseX, List<float> frets) {
        /*
        float dist, point;
        point = frets[0];
        dist = Mathf.Abs(mouseX - frets[0]);

        for (int i = 0; i < frets.Count; i++) {
            if (Mathf.Abs(mouseX - frets[i]) < dist) {
                dist = Mathf.Abs(mouseX - frets[i]);
                point = frets[i];
            }
        }
        */
        int i = getClosestFretIndex(mouseX, frets);
        return frets[i];
    }
    void drawMouseDebugSnap() {
        if (_p1Frets == null || _p2Frets == null) return;

        float mouseX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
        List<float> fretlist;

        if (_turn == 1) fretlist = _p2Frets;
        else fretlist = _p1Frets;

        float fret = snapClosestFret(mouseX, fretlist);

        Debug.DrawLine(new Vector3(fret, -0.05f, 0), new Vector3(fret, 0.05f, 0), Color.white);

        if (Input.GetMouseButton(0)) {
            float prevp1 = _p1;
            float prevp2 = _p2;

            if (_turn == 1) _p2 = fret;
            else _p1 = fret;

            if (getCurrentSemitone() > 88) {
                _p1 = prevp1;
                _p2 = prevp2;
            }
        }
    }

    int getOctaveFromSemitone(int semitone) {
        return semitone / 12 - 1;
    }
    NoteType getNoteFromSemitone(int semitone) {
        return (NoteType)(semitone%12);
    }

    void OnGUI() {
        int semitone = findClosestSemitone(getCurrentNoteFrequency());

        GUI.Label(new Rect(20, 20, 100, 100), semitone.ToString());
        GUI.Label(new Rect(20, 40, 100, 100), getNoteFromSemitone(semitone).ToString()+getOctaveFromSemitone(semitone));
        GUI.Label(new Rect(20, 60, 100, 100), "Player Turn: "+(_turn+1));

        if (_p1Frets != null && _p2Frets != null) {
            GUI.Label(new Rect(20, 80, 1000, 100), "Total number of frets: " +(_p1Frets.Count+_p2Frets.Count));
        }

        GUI.Label(new Rect(20, 100, 100, 100), "_P1: " + _p1);
        GUI.Label(new Rect(20, 120, 100, 100), "Closest: " + _p1Frets[getClosestFretIndex(_p1, _p1Frets)]);
    }

    void updateFretSprites() {
        List<float> frets;
        float endPoint;
        if (_turn == 0) { frets = _p1Frets; endPoint = _p2; }
        else { frets = _p2Frets; endPoint = _p1; }
        if (frets == null) return;

        for (int i = 0; i < _fretSprites.Count; i++) {
            if (i >= frets.Count) {
                _fretSprites[i].SetActive(false); continue;
            }
            float minX, maxX, curX;
            curX = frets[i];
            if (i == 0) minX = 0;
            else minX = frets[i - 1];
            if (i == frets.Count - 1) maxX = 1;
            else maxX = frets[i + 1];

            maxX = curX + Mathf.Abs(curX - maxX) / 2f;
            minX = curX - Mathf.Abs(curX - minX) / 2f;

            if(i==0) minX = curX- Mathf.Abs(curX - maxX) / 2f;
            if(i==frets.Count-1) maxX = curX + Mathf.Abs(curX - minX) / 2f;

            curX = minX + Mathf.Abs(maxX - minX) / 2f;

            float fretWidth = Mathf.Abs(maxX - minX);

            _fretSprites[i].SetActive(true);
            //Debug.Log("Fret width["+i+"]: "+fretWidth);
            _fretSprites[i].transform.localScale = new Vector3(0.01f,0.01f,0.1f);
            _fretSprites[i].transform.localPosition = new Vector3(curX, 0, 0);

            _fretSprites[i].transform.GetChild(1).GetComponent<SpriteRenderer>().size = new Vector2(fretWidth / 0.01f, 2);
            

            _fretSprites[i].transform.GetChild(0).GetComponent<SpriteRenderer>().color = semitoneToColor(getSemitone(Mathf.Abs(frets[i] - endPoint)));
            _fretSprites[i].transform.GetChild(0).GetComponent<SpriteRenderer>().size = new Vector2(1.6f * fretWidth / 0.01f, 2);

        }
    }
    // Update is called once per frame
    void Update()
    {
        /*
        float mouseX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
        _p1 = mouseX;
        _p1 = Mathf.Max(_p1, 0);
        _p1 = Mathf.Min(_p1, _p2-0.01f);
        */

        Debug.DrawLine(new Vector3(0, 0, 0), new Vector3(1, 0, 0));

        //if (Input.GetKeyDown(KeyCode.Alpha1)) drawFretPositions_P1();
        //else if (Input.GetKeyDown(KeyCode.Alpha2)) drawFretPositions_P2();

        drawMouseDebugSnap();

        /*
        if(_turn == 0) drawFretPositions_P1();
        else drawFretPositions_P2();
        */
        

        if (Input.GetKeyDown(KeyCode.LeftArrow)) setSemitoneUp_P2();
        else if (Input.GetKeyDown(KeyCode.RightArrow)) setSemitoneDown_P2();

        if (Input.GetKeyDown(KeyCode.A)) setSemitoneDown_P1();
        else if (Input.GetKeyDown(KeyCode.D)) setSemitoneUp_P1();

        if (Input.GetKeyDown(KeyCode.A) ||
           Input.GetKeyDown(KeyCode.D) ||
           Input.GetKeyDown(KeyCode.LeftArrow) ||
           Input.GetKeyDown(KeyCode.RightArrow) ||
           Input.GetMouseButtonDown(0))
        {
            //this is not draw but recompute
            drawFretPositions_P1();
            drawFretPositions_P2();

            StartCoroutine(MIDIPlayer.playMidiNote(getCurrentSemitone()));
            LevelController.notePlayed(getCurrentSemitone());
            updateFretSprites();
        }

        updatePlayerPositions();
        if (Input.GetKeyDown(KeyCode.Space)) {
            changeTurn();
        }
    }
}
