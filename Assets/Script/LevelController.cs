﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
	public static Song currentSong;

	float currentTime;
	NoteType currentExpectedNote;

	[SerializeField] Transform noteParents;

	[SerializeField] Transform noteObjectPrefab;
	List<Transform> noteObjects = new List<Transform>();

	void Start()
	{
		currentTime = 0;
		currentExpectedNote = NoteType.None;

		currentSong = new Song();

		foreach((NoteType,float) pair in currentSong.songEvents)
		{
			var n = Instantiate(noteObjectPrefab);
			n.SetParent(noteParents);
			n.GetComponent<Image>().color = Color.red;
			n.localPosition = new Vector3(100 * pair.Item2, 0);
			noteObjects.Add(n);
		}
	}

	bool songPlayed = false;

	void Update()
	{
		currentTime += Time.deltaTime;

		noteParents.localPosition = new Vector3(noteParents.localPosition.x - 100*Time.deltaTime, noteParents.localPosition.y);

		if (!songPlayed)
		{
			foreach((NoteType,float) pair in currentSong.songEvents)
			{
				if (currentTime < pair.Item2)
				{
					if (currentExpectedNote != pair.Item1) {
						StartCoroutine(MIDIPlayer.playMidiNote((int)currentExpectedNote+48));
						currentExpectedNote = pair.Item1;
					}
					break;
				}
			}
			if (currentTime > currentSong.songEvents[currentSong.songEvents.Count-1].Item2) 
			{
				songPlayed = false;
			}
		}
		else
		{

		}
	}

	public static List<int> playedSemitones = new List<int>();

	public static void notePlayed(int semitone)
	{
		if (playedSemitones.Count <= 10)
		{
			playedSemitones.Add(semitone);
			foreach((NoteType,float) pair in currentSong.songEvents)
			{
				if (semitone%12 == (int)pair.Item1)Debug.Log("Right!" + ((NoteType)(semitone%12)));
				else Debug.Log("Wrong" + ((NoteType)(semitone%12)));
			}
		}
	}
}
