﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MusicPlayer : MonoBehaviour
{
	[SerializeField] Toggle LogChordNames;

	public void PlayChords(List<int> chords)
	{
		StartCoroutine(PlayChordsRoutine(chords));
	}

	private IEnumerator PlayChordsRoutine(List<int> chords)
	{
		foreach (int chord in chords)
		{
			PlayChord(chord);
			yield return new WaitForSeconds(2f);
		}
	}

	public void PlayChord(int chord)
	{
		// if(LogChordNames.isOn)
		StartCoroutine(PlayChordRoutine(chord));
	}

	private IEnumerator PlayChordRoutine(int chord)
	{
		StartCoroutine(MIDIPlayer.playMidiNote(10));
		yield return new WaitForSeconds(.1f);
	}

	public void PlayScale(List<int> semitones)
	{
		StartCoroutine(PlayScaleRoutine(semitones));     
	}

	private IEnumerator PlayScaleRoutine(List<int> semitones)
	{
		foreach (int i in semitones)
		{
			int noteToPlay = i + 36;
			StartCoroutine(MIDIPlayer.playMidiNote(noteToPlay));
			yield return new WaitForSeconds(.5f);
		}
	}
}

public class PlayChordByDegree : UnityEvent<int>
{
}

public static class PlayEvents
{
	public static PlayChordByDegree _playChord = new PlayChordByDegree();

	public static void PlayChord(int i){ _playChord.Invoke(i); }

}
